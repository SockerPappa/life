__author__ = 'dod'

import pprint


class Matrix(object):

    def __init__(self, cols, rows, default=False):
        self.num_rows = rows
        self.num_cols = cols
        self.cells = [[default for c in range(cols)] for r in range(rows)]

    def get_num_rows(self):
        return self.num_rows
    def get_num_columns(self):
        return self.num_cols
    def __str__(self):
        return pprint.pformat(self.cells)
    def get_row(self, y):
        return self.cells[y]
    def get(self, x, y):
        return self.cells[y][x]

    def update(self, x, y, val):
        self.cells[y][x] = val
    def __eq__(self, other):
        if type(other) is type(self):
            if isinstance(other, self.__class__):
                return self.__dict__ == other.__dict__
        return False
    def __ne__(self, other):
        return not self.__eq__(other)
